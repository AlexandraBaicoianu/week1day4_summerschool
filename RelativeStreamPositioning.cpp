#include <fstream>
#include <iostream>
void main()
{
	std::ofstream ofs("test.txt");
	if (ofs.is_open()) {
		std::cout << "File was opened, position is:" << ofs.tellp() << std::endl;

		ofs << "Curious minds";
		std::cout << "New postion is: " << ofs.tellp() << std::endl;

		ofs.seekp(-5, ofs.end);
		ofs << "M";
		ofs.close();
	}
}