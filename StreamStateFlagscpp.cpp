#include <fstream>
#include <iostream>
void main()
{
	std::ifstream file;
	file.open("test.txt");
	if (file.is_open()) {
		std::cout << "File opened successfully" << std::endl;
		char szTextLine[64];
		while (file.getline(szTextLine, 64)) {
			std::cout << ">" << szTextLine << "<, " << file.tellg() << std::endl;
		}
		if (file.eof()) std::cout << "Reached EOF" << std::endl;
		else std::cout << "Error: " << file.badbit << std::endl;
		file.close();
	}
	else
		std::cout << "Could not open file: " << "test.txt"<< std::endl;
}